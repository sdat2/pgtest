"""

"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import xarray as xr
import dask as ds
import cartopy.crs as ccrs
import xarray.plot as xplt
import matplotlib.pyplot as plt
import gcsfs
import intake

cat = intake.Catalog("https://raw.githubusercontent.com/pangeo-data/"
                      + "pangeo-datastore/master/intake-catalogs/ocean/llc4320.yaml")

# cat = intake.Catalog("https://raw.githubusercontent.com/pangeo-data/"
#                      + "pangeo-datastore/master/intake-catalogs/ocean/GFDL_CM2.6.yaml")

print(cat)
print(cat.yaml())
print(cat.walk(depth=5))

# ds = cat["GFDL_CM2_6_control_ocean_surface"].to_dask()
ds = cat["LLC4320_SSH"].to_dask()  # .to_array()
# print(ds.__repr__())
ds.plot.scatter()

"""
for key in cat:
    print(key)
    ds = cat[key].to_dask()  # .to_array()
    # print(ds.__repr__())
    ds.plot.scatter()
    ds.show()
"""