"""
Examples of how to plot data from xarrays taken from
http://xarray.pydata.org/en/stable/plotting.html
and then refactored.

"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import xarray as xr
import cartopy.crs as ccrs
import xarray.plot as xplt
from utilities import my_plotting_style as mps
from utilities import my_wrappers as mwr
mps.set_defaults(quality='high')

airtemps = xr.tutorial.open_dataset('air_temperature')
air = airtemps.air - 273.15
air.attrs = airtemps.air.attrs
air.attrs['units'] = 'deg C'
air1d = air.isel(lat=10, lon=10)
air1d.plot()
plt.show()  ##
print(air1d.attrs)
air1d[:200].plot.line('b-^')
plt.show()  ##
air1d[:200].plot.line(color='purple', marker='o')
plt.show()  ##
fig, axes = plt.subplots(ncols=2)
air1d.plot(ax=axes[0])
air1d.plot.hist(ax=axes[1])
plt.tight_layout()
plt.draw()
air1d.plot(aspect=2, size=3)
plt.tight_layout()
plt.show()  ##
air.isel(lon=10, lat=[19, 21, 22]).plot.line(x='time')
air.isel(time=10, lon=[10, 11]).plot(y='lat', hue='lon')
plt.show()   ##
air1d[:20].plot.step(where='mid')
air.isel(time=10, lon=[10, 11]).plot.line(y='lat', hue='lon', xincrease=False, yincrease=False)
air2d = air.isel(time=500)
air2d.plot()
air2d.plot(yincrease=False)
plt.show()   ##
bad_air2d = air2d.copy()
bad_air2d[dict(lat=slice(0, 10), lon=slice(0, 25))] = np.nan
bad_air2d.plot()
b = air2d.copy()
b.coords['lat'] = np.log(b.coords['lat'])
b.plot()
plt.show()   ##
air2d.plot(cmap=plt.cm.Blues)
plt.title('These colors prove North America\nhas fallen in the ocean')
plt.ylabel('latitude')
plt.xlabel('longitude')
plt.tight_layout()
plt.draw()
plt.show()   ##
airtemps.air.isel(time=0).plot()
air_outliers = airtemps.air.isel(time=0).copy()
air_outliers[0, 0] = 100
air_outliers[-1, -1] = 400
air_outliers.plot()
plt.show()   ##

air_outliers.plot(robust=True)
plt.show()   ##

air2d.plot(levels=8)
air2d.plot(levels=[0, 12, 18, 30])
flatui = ["#9b59b6", "#3498db", "#95a5a6", "#e74c3c", "#34495e", "#2ecc71"]
air2d.plot(levels=[0, 12, 18, 30], colors=flatui)
air2d.plot(levels=10, cmap='husl')
plt.draw()
plt.show()   ##

t = air.isel(time=slice(0, 365 * 4, 250))
g_simple = t.plot(x='lon', y='lat', col='time', col_wrap=3)
g_simple_line = t.isel(lat=slice(0,None,4)).plot(x='lon', hue='lat', col='time', col_wrap=3)
t2 = t.isel(time=slice(0, 2))
t4d = xr.concat([t2, t2 + 40], pd.Index(['normal', 'hot'], name='fourth_dim'))
t4d.plot(x='lon', y='lat', col='time', row='fourth_dim')
hasoutliers = t.isel(time=slice(0, 5)).copy()
hasoutliers[0, 0, 0] = -100
hasoutliers[-1, -1, -1] = 400
g = hasoutliers.plot.pcolormesh('lon', 'lat', col='time', col_wrap=3,
                                robust=True, cmap='viridis',
                                cbar_kwargs={'label': 'this has outliers'})
g = t.plot.imshow('lon', 'lat', col='time', col_wrap=3, robust=True)
for i, ax in enumerate(g.axes.flat):
    ax.set_title('Air Temperature %d' % i)
bottomright = g.axes[-1, -1]
bottomright.annotate('bottom right', (240, 40))
plt.draw()
plt.show()   ##

ds = xr.tutorial.scatter_example_dataset()
ds.plot.scatter(x='A', y='B')
ds.plot.scatter(x='A', y='B', hue='w')
ds.w.values = [1, 2, 3, 5]
ds.plot.scatter(x='A', y='B', hue='w', hue_style='discrete')
ds.plot.scatter(x='A', y='B', col='x', row='z', hue='w', hue_style='discrete')
air = xr.tutorial.open_dataset('air_temperature').air
ax = plt.axes(projection=ccrs.Orthographic(-80, 35))
air.isel(time=0).plot.contourf(ax=ax, transform=ccrs.PlateCarree())
ax.set_global()
ax.coastlines()
p = air.isel(time=[0, 4]).plot(transform=ccrs.PlateCarree(), col='time',
                               subplot_kws={'projection': ccrs.Orthographic(-80, 35)})
for ax in p.axes.flat:
    ax.coastlines()
    ax.gridlines()

plt.draw()
plt.show()   ##

da = xr.DataArray(range(5))
fig, axes = plt.subplots(ncols=2, nrows=2)
da.plot(ax=axes[0, 0])
da.plot.line(ax=axes[0, 1])
xplt.plot(da, ax=axes[1, 0])
xplt.line(da, ax=axes[1, 1])
plt.tight_layout()
plt.draw()
plt.show()   ##

a0 = xr.DataArray(np.zeros((4, 3, 2)), dims=('y', 'x', 'z'),
                  name='temperature')
a0[0, 0, 0] = 1
a = a0.isel(z=0)
a.plot()
lon, lat = np.meshgrid(np.linspace(-20, 20, 5), np.linspace(0, 30, 4))
lon += lat/10
lat += lon/10
da = xr.DataArray(np.arange(20).reshape(4, 5), dims=['y', 'x'],
                  coords = {'lat': (('y', 'x'), lat),
                            'lon': (('y', 'x'), lon)})
da.plot.pcolormesh('lon', 'lat')
ax = plt.subplot(projection=ccrs.PlateCarree())
da.plot.pcolormesh('lon', 'lat', ax=ax)
ax.scatter(lon, lat, transform=ccrs.PlateCarree())
ax.coastlines()
ax.gridlines(draw_labels=True)
ax = plt.subplot(projection=ccrs.PlateCarree())
da.plot.pcolormesh('lon', 'lat', ax=ax, infer_intervals=True)
ax.scatter(lon, lat, transform=ccrs.PlateCarree())
ax.coastlines(); ax.gridlines(draw_labels=True)
f, ax = plt.subplots(2, 1)
da.plot.line(x='lon', hue='y', ax=ax[0])
da.plot.line(x='lon', hue='x', ax=ax[1])
plt.show()   ##
