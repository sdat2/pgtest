import intake
cat = intake.Catalog("https://raw.githubusercontent.com/pangeo-data/pangeo-datastore/master/intake-catalogs/ocean/channel.yaml")
print(cat.walk(depth=5))
ds = cat["MITgcm_channel_flatbottom_02km_run01_phys-mon"].to_dask()