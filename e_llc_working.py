"""
Remember 

pip install gcsfs==0.3.0

"""
import intake
import gcsfs
import xarray as xr
import xgcm
from xmitgcm import llcreader
import matplotlib.pyplot as plt
from utilities import my_plotting_style as mps
from utilities import my_wrappers as mwr
mps.set_defaults()


@mwr.timeit
def read_data():
    """

    :return:
    """
    cat_url = 'https://raw.githubusercontent.com/pangeo-data/pangeo-datastore/master/intake-catalogs/master.yaml'
    cat = intake.Catalog(cat_url)
    ssh = cat['ocean.LLC4320.LLC4320_SSH'].to_dask()
    sst = cat.ocean.LLC4320.LLC4320_SST.to_dask()
    u = cat.ocean.LLC4320.LLC4320_SSU.to_dask()
    v = cat.ocean.LLC4320.LLC4320_SSV.to_dask()
    return ssh, sst, u, v


@mwr.timeit
def plot_eta(ssh):
    """

    :return:
    """
    fig = plt.figure(figsize=[10, 5])
    ssh.Eta.sel(face=10, time=0, method='nearest').plot()
    fig = mps.defined_size(fig, 'sc')
    plt.savefig('nice.png', bbox_inches='tight')


@mwr.timeit
def merge(ssh, sst, u, v):
    """

    :param ssh:
    :param sst:
    :param u:
    :param v:
    :return:
    """
    ds = xr.merge([ssh, sst, u, v])
    ds = llcreader.llcmodel.faces_dataset_to_latlon(ds, metric_vector_pairs=[])
    return ds
    #coords = cat.LLC4320_grid.to_dask().reset_coords()


def run_all():
    ssh, sst, u, v = read_data()
    plot_eta(ssh)
    ds = merge(ssh, sst, u, v)

run_all()

"""
Dimensions:  (face: 13, i: 4320, j: 4320, time: 9030)
Coordinates:
  * face     (face) int64 0 1 2 3 4 5 6 7 8 9 10 11 12
  * i        (i) int64 0 1 2 3 4 5 6 7 ... 4313 4314 4315 4316 4317 4318 4319
  * j        (j) int64 0 1 2 3 4 5 6 7 ... 4313 4314 4315 4316 4317 4318 4319
  * time     (time) datetime64[ns] 2011-09-13 ... 2012-09-23T05:00:00
Data variables:
    Eta      (time, face, j, i) float32 dask.array<chunksize=(1, 1, 4320, 4320), meta=np.ndarray>
Attributes:
    Conventions:  CF-1.6
    history:      Created by calling `open_mdsdataset(llc_method='smallchunks...
    source:       MITgcm
    title:        netCDF wrapper of MITgcm MDS binary data

name: LLC4320_SSH
container: xarray
plugin: ['zarr']
description: MITgcm LLC4320 Ocean Simulation Sea Surface Height
direct_access: forbid
user_parameters: []
metadata: 
  url: http://online.kitp.ucsb.edu/online/blayers18/menemenlis/
  tags: ['ocean', 'model']
args: 
  urlpath: gcs://pangeo-data/llc4320_surface/Eta
  storage_options: 
    token: anon
  consolidated: True

"""

